import controlP5.*;


ControlP5 cp5;

Knob speedKnob;
Knob VoltageKnob;

int bg = 0;


void setup() 
{
  size(700,400);
  smooth();
  noStroke();
  
  cp5 = new ControlP5(this);
                     
  speedKnob = cp5.addKnob("ADC_speed")
                 .setViewStyle(Knob.ELLIPSE)
                 .setRange(1,4)
                 .setValue(4)
                 .setPosition(width/2,height/4)
                 .setRadius(50)
                 .setNumberOfTickMarks(3)
                 .setTickMarkLength(4)
                 .setTickMarkWeight(5)
                 .snapToTickMarks(true)
                 .setColorForeground(color(255,0,0))  //RED
                 .setColorBackground(color(0, 160, 100)) //GREEN
                 .setColorActive(color(255,255,0)) //YELLOW
                 .setDragDirection(Knob.HORIZONTAL)
                 ;

  speedKnob = cp5.addKnob("Voltage_Range")
                 .setViewStyle(Knob.ARC)
                 .setRange(1,3)
                 .setValue(3)
                 .setPosition(50,height/4)
                 .setRadius(100)
                 .setNumberOfTickMarks(2)
                 .setTickMarkLength(4)
                 .setTickMarkWeight(20)
                 .snapToTickMarks(true)
                 .setColorForeground(color(200))
                 .setColorBackground(color(0, 0, 255))
                 .setColorActive(color(255,0,0))
                 .setDragDirection(Knob.HORIZONTAL)
                 ;                 
}

void draw() 
{
  background(bg);
}

void ADC_speed(int theValue)
{
  bg = 10*theValue;
  println("ADC Speed changed to "+theValue);
}

void Voltage_Range(int theValue)
{
  bg = 25*theValue;
  println("Voltage range changed to "+theValue);
}



