int height=600;
int width=600;
static int v=20;


void setup() //happens once
{
  size(width,height);
  noLoop();
}

void draw()  //continuously loops
{
  int rgb=0;
  background(0);

  for(int x=0; x<width; x+=v)
     {
       for(int y=0; y<height; y+=v)
          {
          drawEllipse(x,y,rgb);
          }
       rgb++;
     }
}

void drawEllipse(int x, int y, int rgb) //custom function
{
  fill(rgb);
  ellipse(x, y, v, v);
}

