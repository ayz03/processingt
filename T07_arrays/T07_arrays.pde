//DECLARE
Ball[] balls = new Ball[50];
void setup()
{
  //size(displayWidth,displayHeight);
  size(600, 600);
  smooth();
  //INITIALIZE
  for (int i=0; i<balls.length; i++)
  {
    balls[i] = new Ball(random(0, width), random(0, 300));
  }
}

void draw()
{
  background(0);
  //ellipse(width/2,height/2,300,300); 

  //CALL FUNCTIONS
  for (int i=0; i<balls.length; i++)
  {
    balls[i].run();
  }
}

