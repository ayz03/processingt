import toxi.geom.*;

String[] csvData;
void setup()
{
  size(600,600);
  background(0);
  csvData = loadStrings("data/Mouse_points.csv");
  fill(0,255,0);
}


void draw()
{
  for(int i=0; i<csvData.length; i++)
  {
    println(csvData[i]);
    String[] csvFields = split(csvData[i],',');
    int x = int(csvFields[1]);
    int y = int(csvFields[2]);
    update(x,y);
  }
}

Vec3D point = new Vec3D();

void update(int _x, int _y)
{
   point.x = _x;
   point.y = _y;
   ellipse(point.x,point.y,20,20); 
}
