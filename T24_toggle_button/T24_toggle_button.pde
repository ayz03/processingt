
import controlP5.*;

ControlP5 cp5;

int col = color(255);

void setup() 
{
  size(400,400);
  smooth();
  cp5 = new ControlP5(this);
  
  cp5.addToggle("Pause")
     .setPosition(40,250)
     .setSize(50,20)
     .setValue(true)
     //.setMode(ControlP5.SWITCH)
     ;
}
  

void draw() 
{
  background(col);
}



void Pause(boolean theFlag) 
{
  if(theFlag==true) 
  {
    col = color(255);
  } else 
  {
    col = color(0);
  }
  println("a toggle event.");
}

