import processing.serial.*;

Serial myPort;     //object from the Serial class
Serial[] portList;
//int i;
 
void setup()
{
  //size(600,600);
  //println(Serial.list());
  //String portName = Serial.list()[0];
  //myPort = new Serial(this, portName, 9600); 
  //ports = Serial.list();
  //myPort = new Serial(this, "COM6", 9600);


    String[] portNames;
    portNames = Serial.list();  //we have the strings named ports like "COM6" "COM7"
    portList = new Serial[portNames.length];    
    for(int i=0; i<portNames.length; i++)
    {
      print("trying:");
      println(portNames[i]);
      //myPort = new Serial(this,Serial.list()[i],9600);
      myPort = new Serial(this,portNames[i],9600);
      myPort.clear();
      myPort.bufferUntil('\n');
      //delay(500);
      if(myPort.available()>0)
        {
          String in = myPort.readStringUntil('\n');
          if(in == "Uno")
          {
            print("Port Found:");
            println(portNames[i]);
            myPort.write('P');
            myPort.write('C');
            myPort.write('\n');
            break;          
          }
        }
    } 
 
}

void draw()
{
/*  while(myPort.available()>0)
    {
      String in = myPort.readStringUntil('\n');
      if(in == "Uno")
      {
        myPort.write('P');
        myPort.write('C');
        myPort.write('\n');
        break;          
      }
    }   */
  String val = "NULL";
  if(myPort.available() > 0) // If data is available,
  {  
  val = myPort.readStringUntil('\n');  // read it and store it in val
  } 
  println(val); //print it out in the console
}



