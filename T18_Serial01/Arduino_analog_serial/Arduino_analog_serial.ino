const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

void setup() 
{
  Serial.begin(9600);
  boolean ok = false;
  while(!ok)
  {
    Serial.println("Uno");
    if(Serial.available()>0)
      {
        String in = Serial.readStringUntil('\n');
        if(in == "PC")
          ok = true;
      }
  }
}

void loop() 
{
  sensorValue = analogRead(analogInPin); // read the analog in value:
  outputValue = map(sensorValue, 0, 1023, 0, 255); // map it to the range of the analog out:
  Serial.println(outputValue);
  delay(2);
}
