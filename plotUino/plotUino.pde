//screen offsets
int dx; 
int dy;

int div_PX;
int samples;    //no of samples to be stored in the array

int width  = 1024;
int height = 800;

final int Y_DIV = 12;
final int CMD_WIN_DIV = 8;
final int AXIS_LABEL_DIV = 50;

int plotWidth;
int plotHeight;

float ver = 1.0;

int bgColor = 255; //background color

PImage logo;

int STATE_FIRST_RUN = 0;
int appState = STATE_FIRST_RUN;

void setup()
{
  prepareScreen();
  clearPlotArea();
  printLogo();
  //printMainText("plotUino"+str(ver));
  delay(2000);   
}

void draw()
{
  if(appState == STATE_FIRST_RUN)
  {   
    drawAxis();
  }
}


//sub functions

void prepareScreen()
{
  size(width,height);
  background(bgColor);
  println("screen Prepared");
    
  dx = width/AXIS_LABEL_DIV;
  dy = dx+5; 
  plotWidth = width-floor(width/CMD_WIN_DIV); println(plotWidth);
  plotWidth = plotWidth - dx;  println(plotWidth);
  plotHeight = height - dy;    println(plotHeight);
  div_PX = floor(plotHeight/Y_DIV);    println(div_PX);
}

void clearPlotArea()  // a part of the screen gets cleared
{
  noStroke();
  fill(0);
  rect(dx,0,plotWidth,plotHeight);    
}

void printLogo()
{
  logo = loadImage("plotUino_icon.png");
  logo.resize(width-plotWidth,height/5);
  image(logo,plotWidth+5,0,logo.width,logo.height);   
}

void printMainText(String txt) //print Text in front 
{
  fill(245);
  textSize(50);
  text(txt,logo.width+2,logo.height/2); 
}

void drawAxis()
{
 for(int i = 0; i<=Y_DIV; i++)
 {
    if(i==6)
        stroke(255,0,0);
    else
        stroke(150);
    line(dx,(i*div_PX),plotWidth,(i*div_PX));    
 }
 for(int x = dx; x <plotWidth; x+=div_PX)
 {
    line(x,0,x,plotHeight);
    samples=(x-dx);
 }
 println(samples);
}


