class Ball
{
  //GLOBAL VARIABLES  
  //float x = 0;
  //float y = 0;
  //float speedX = random(-2,2);
  //float speedY = random(-2,2);
  
  Vec3D loc = new Vec3D(0,0,0);
  Vec3D speed = new Vec3D(1,0,0);
  Vec3D grav = new Vec3D(0,0.2,0);  
  
  //PVector  loc = new PVector(0,0,0);
  //PVector speed = new PVector(1,2.3,0);

  //CONSTRUCTORS  
  /*Ball(float _x, float _y)
  {
    loc.x = _x;
    loc.y = _y;
  }*/
  
  Ball(Vec3D _loc)
  {
    loc=_loc;
  } 

  //FUNCTIONS

  void run()
  {
    displayBall(); //Display
    move(); //move the ball
    bounce(); //condition the x,y coordinates to bounce the ball
    gravity(); //function that pulls down the ball
  }
  void gravity()
  {
    speed.addSelf(grav);
  }
  void bounce()
  {
    if(loc.x>width) speed.x *= -1;
    if(loc.x<0)     speed.x *= -1;
    if(loc.y>height) speed.y *= -1;
    if(loc.y<0)      speed.y *= -1;    
  }
  void move()
  {
    //x+=speedX;
    //y+=speedY;
   loc.addSelf(speed); 
  }
  void displayBall()
  {
    ellipse(loc.x, loc.y, 20, 20);
  }
}

