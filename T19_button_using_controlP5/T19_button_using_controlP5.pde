import controlP5.*;

ControlP5 ButtonP5;

int width  = 1024;
int height = 800;

int Button0_color = 100;
int Button1_color = 150;
int Button2_color = 200;

int bgColor = 0;


void setup()
{
  size(width,height);
  
  ButtonP5 = new ControlP5(this);
  
  ButtonP5.addButton("Button0")
          .setValue(Button0_color)
          .setId(0)
          .setPosition(100,100)
          .setSize(200,19);
  ButtonP5.addButton("Button1")
          .setValue(Button1_color)
          .setId(1)
          .setPosition(100,120)
          .setSize(200,19);
  ButtonP5.addButton("Button2")
          .setValue(Button2_color)
          .setId(2)
          .setPosition(100,140)
          .setSize(200,19);          
}

void draw()
{
  background(bgColor); 
}

void controlEvent(ControlEvent theEvent)
{
  println("got a control event from controller");
  println(theEvent.getController().getLabel()+" was pressed and "+
          theEvent.getController().getValue()+" value is got for background");   
  
  switch(theEvent.getController().getId())
        {
           case 0: bgColor = Button0_color; break;
           case 1: bgColor = Button1_color; break;
           case 2: bgColor = Button2_color; break;
           default: ; break;
        }
}
