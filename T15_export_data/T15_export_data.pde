PrintWriter csvWriter;
void setup()
{
  size(600,600);
  csvWriter = createWriter("data/Mouse_points.csv");
  fill(0,255,0);
}

int n=0;

void draw()
{
  background(0);
  ellipse(mouseX,mouseY,20,20);
  csvWriter.println(n+","+mouseX+","+mouseY);
  if(mousePressed)
    {
      fill(255,0,0);
      csvWriter.flush();
      csvWriter.close();
    }
  n++;
}
