import javax.swing.*; 

SecondApplet s;   

String msg;
void setup() 
{
  size(640, 480);
  PFrame f = new PFrame(800, 80);
  frame.setTitle("oscilloscope");
  f.setTitle("user guide");
  fill(0);
}

long ms = 0;
void draw() 
{
  background(0);
  ms = millis();
  msg = "Press the Connect button " + str(ms);
  delay(1000);
}

public class PFrame extends JFrame 
{
  public PFrame(int width, int height) 
  {
    setBounds(0, 300, width, height);
    s = new SecondApplet();
    add(s);
    s.init();
    show();
  }
}
public class SecondApplet extends PApplet 
{
PFont f;                          // STEP 2 Declare PFont variable

  void setup()
  {
    background(255);
    f = createFont("Arial",24,true); // STEP 3 Create Font
    textFont(f,16);                 // STEP 4 Specify font to be used
   // text("Hello Strings!",0,30);  // STEP 6 Display Text 
  }
  
  void draw() 
  {
    background(255);
    fill(0);                        // STEP 5 Specify font color   
    text(msg,0,30);  // STEP 6 Display Text  
  }  
}








