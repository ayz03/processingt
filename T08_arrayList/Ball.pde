class Ball
{
  //GLOBAL VARIABLES  
  float x = 0;
  float y = 0;
  float speedX = random(-1,1);
  float speedY = random(-1,1);

  //CONSTRUCTORS  
  Ball(float _x, float _y)
  {
    x=_x;
    y=_y;
  }

  //FUNCTIONS

  void run()
  {
    displayBall(); //Display
    move(); //move the ball
    bounce(); //condition the x,y coordinates to bounce the ball
    gravity(); //function that pulls down the ball
  }
  void gravity()
  {
    speedY += 0.98;
  }
  void bounce()
  {
    if(x>width) speedX *= -1;
    if(x<0)     speedX *= -1;
    if(y>height) speedY *= -1;
    if(y<0)      speedY *= -1;    
  }
  void move()
  {
    x+=speedX;
    y+=speedY; 
  }
  void displayBall()
  {
    ellipse(x, y, 20, 20);
  }
}

