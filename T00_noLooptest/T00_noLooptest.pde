int[] i ;
int n=0;
void setup()
{
  width=200;
  height=200;
  size(width,height);
  background(0);
  i = new int[width];
  for(int n=0; n<i.length; n++)
     {
       i[n]=n;
     }
  noLoop();  //causes draw to only execute once   
}

void draw()
{
  //for(int n=0; n<i.length; n++)
     //{
       if(drawMode)
         {
           stroke(255);
           point(n,i[n]);
           drawMode=false;
         }
       
       //if(n==0)
        // noLoop();
       //myDelay(1000); 
     //}
}

/*void myDelay(int ms)
{
   try
  {    
    Thread.sleep(ms);
  }
  catch(Exception e){}
}*/

boolean drawMode = false;

void mousePressed()
{
  drawMode = true;
  n++;
  redraw();
}
