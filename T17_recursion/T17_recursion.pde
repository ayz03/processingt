import toxi.geom.*;

void setup()
{
  size(600,600);
  smooth();
}

void draw()
{
  background(0);
  Vec3D a = new Vec3D(0, height/2.0,0);
  Vec3D b = new Vec3D(width, height/2.0,0);
  recursiveCircle(a,b,10);
}

void recursiveCircle(Vec3D v1, Vec3D v2, int iterations)
{
  if(iterations > 0)
  {
    float diff = v1.distanceTo(v2);
    Vec3D midP = v1.add(v2);
    midP.scaleSelf(0.5);
    
    noFill();
    stroke(255);
    ellipseMode(CENTER);
    ellipse(midP.x, midP.y, diff, diff); 
    
    recursiveCircle(v1,midP,iterations-1);     
    recursiveCircle(midP,v2,iterations-1);    
  }  
}
