import processing.serial.*;

Serial myPort;       

String[] portNames;
boolean portFound = false;

static final int TIMEOUT0 = 10000;
static final int TIMEOUT1 = 3000;
static final int TIMEOUT2 = 500;
//static final int lf = 10;    // Linefeed in ASCII
//String myString = null;

void setup()
{
  portNames = Serial.list();
  //println(Serial.list());
  println(portNames[0]);
  println(portNames[1]);
  
  println(portNames.length);  
 
 //port finding loop
  int startTime0 = millis();
  while((millis()-startTime0)<TIMEOUT0)
  {
      for(int i=0; i<portNames.length; i++)
      {
          print("trying to communicate with");
          println(portNames[i]);
          myPort = new Serial(this, Serial.list()[i], 9600);
          int startTime1 = millis();
          myPort.clear();
          while((millis()-startTime1)<TIMEOUT1)
          {
              if(myPort.available()>0)
              {
                //myString = myPort.readStringUntil(lf);
                //myString = myPort.readString();
               
                char inByte = myPort.readChar();
                //if(myString == "Uno")
                if(inByte == 'U')
                {
                  portFound = true;
                  myPort.clear();
                  break;             //break from timeout
                }                
              }
    
          }
          if(portFound == true)
          {
            println("port is found");
            int startTime2 = millis();
            while((millis()-startTime2)<TIMEOUT2)
            {
              myPort.write('C');
              delay(10);
            }
            myPort.clear();
            break;                 //break from for loop 
          }                   
          else
            {
            myPort.stop();
            println("port not found Retrying with bext available port...");
            }   
      }     
      if(portFound == true)
      break;
  }

  if(portFound == true)
  {
    println("port is found Draw can have mcu data");
    myPort.clear();
    myPort.bufferUntil('\n');
  }
  else  
  {
    println("RECONNECT");
  }
}

void draw()
{ 
}

void serialEvent(Serial myPort)
{
  String inString = myPort.readStringUntil('\n');
  if( inString != null)
  {
    inString = trim(inString);
    float inData = float(inString);
    println(inData);
    //inByte = map(inByte,0,1023,0,height);
  }
}

//myPort = new Serial(this, Serial.list()[0], 9600);
  
//myPort.write(65);
