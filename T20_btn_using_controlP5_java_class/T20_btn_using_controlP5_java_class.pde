//package t20cmdbox;

import controlP5.ControlEvent;


  public static int width = 1024;
  public static int height = 800;
  
  public static int bgColor = 0;
  
  private CMDbox c;

  void setup() 
  {
    size(1024,800);
    c = new CMDbox(this);
    c.createCMDbox();
  }

  void draw() 
  {
      background(bgColor);
  }
  
  public void controlEvent(ControlEvent theEvent)
  {
    println("got a control event from controller");
    println(theEvent.getController().getLabel()+" was pressed and "+
              theEvent.getController().getValue()+" value is got for background");   
      
      switch(theEvent.getController().getId())
            {
               case 0: bgColor = 100; break;
               case 1: bgColor = 200; break;
               case 2: bgColor = 300; break;
               default: ; break;
            }
   }  

