import controlP5.ControlEvent;
import controlP5.ControlP5;
import processing.core.PApplet;

public class CMDbox 
{
  PApplet parent;
  
  
  private static int NO_OF_BUTTONS = 5;
  private static int CMD_WIN_DIV = 5;
  
  private static int SPACE_BTWN_BTNS = 3; //in px
  
  private int cmdbox_width = 0;
  private int cmdbox_height = 0;
  
  private int btn_height = 0;
  private int btn_width = 0;
  
  private ControlP5 ButtonP5;
  
  public CMDbox(PApplet _p)
  {
    parent = _p;
    
    cmdbox_width = (parent.width/CMD_WIN_DIV);
    cmdbox_height = parent.height;
    
    btn_width = cmdbox_width - SPACE_BTWN_BTNS;
    btn_height = (int)(cmdbox_height/NO_OF_BUTTONS)- SPACE_BTWN_BTNS;

    ButtonP5 = new ControlP5(parent);
  }
  
  public int getLeftWidth()
  {
    int leftW = parent.width - cmdbox_width;
    return leftW;    
  }
  
  public void createCMDbox()
  {
      ButtonP5.addButton("Button0")
          .setValue(100)
          .setId(0)
          .setPosition(100,100)
          .setSize(200,19);
          ButtonP5.addButton("Button1")
          .setValue(200)
          .setId(1)
          .setPosition(100,120)
          .setSize(200,19);
          ButtonP5.addButton("Button2")
          .setValue(300)
          .setId(2)
          .setPosition(100,140)
          .setSize(200,19);      
    
  }
  
  public void setLabels(int btn_id_no,String s)
  {
    if(btn_id_no<NO_OF_BUTTONS)
    { 
      
    }
  }
  


}

