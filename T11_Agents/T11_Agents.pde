import toxi.geom.*; 

//DECLARE
ArrayList ballCollections;
void setup()
{
  size(600,600);
  smooth();
  ballCollections = new ArrayList();
  
  for(int i = 0;i < 200; i++)
  {
    Vec3D _loc = new Vec3D(random(0,width),random(0,200),0);
    Ball myBall = new Ball(_loc);
    ballCollections.add(myBall);
  }
}

void draw()
{
  background(0);

  for(int i=0; i<ballCollections.size(); i++)
     {    
      Ball tmp_b = (Ball)ballCollections.get(i);
      tmp_b.run();   
     }
}
