int x = 0;
int y = 300;

int speed = 2;

void setup() //happens once
{
  size(600,600);
}

void draw()  //continuously loops
{
  background(0);
  ellipse(x,y,20,20);
  x+=speed;
  if(x>width)
    {
      x=0;
    }
}
