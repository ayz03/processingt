public class Ball
{
  //GLOBAL VARIABLES  
  private float x = 0;
  private float y = 0;
  private float speedX = 5;
  private float speedY = 1.5;

  //CONSTRUCTORS  
  public Ball(float _x, float _y)
  {
    x=_x;
    y=_y;
  }

  //FUNCTIONS

  public void run()
  {
    displayBall(); //Display
    move(); //move the ball
    bounce(); //condition the x,y coordinates to bounce the ball
    gravity(); //function that pulls down the ball
  }
  private void gravity()
  {
    speedY += 0.98;
  }
  private void bounce()
  {
    if(x>width) speedX *= -1;
    if(x<0)     speedX *= -1;
    if(y>height) speedY *= -1;
    if(y<0)      speedY *= -1;    
  }
  private void move()
  {
    x+=speedX;
    y+=speedY; 
  }
  private void displayBall()
  {
    ellipse(x, y, 20, 20);
  }
}

