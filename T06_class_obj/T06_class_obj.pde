//DECLARE
Ball b,b1,b2;
void setup()
{
//size(displayWidth,displayHeight);
  size(600,600);
  smooth();
//INITIALIZE
  b = new Ball(200,100);
  b1 = new Ball(0,0);
  b2 = new Ball(150,300);
}

void draw()
{
  background(0);
  //ellipse(width/2,height/2,300,300); 

//CALL FUNCTIONS 
  b.run();
  b1.run();
  b2.run();
}
