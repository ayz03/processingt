int height=600;
int width=600;
static int v=20;


void setup() //happens once
{
  size(width,height);
}

void draw()  //continuously loops
{
  background(0);
  for(int x=0; x<width; x+=v)
     {
     if(x<200)
       fill(255,0,0);
     else if((199<x)&&(x<400))
       fill(0,255,0);
     else
       fill(0,0,255);       
     drawEllipse(x,200);
     }
}

void drawEllipse(int x, int y) //custom function
{
  ellipse(x, y, v, v);
}

