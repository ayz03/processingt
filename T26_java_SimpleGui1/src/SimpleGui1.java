import javax.swing.*;
import java.awt.event.*;


public class SimpleGui1 extends JFrame implements ActionListener      //implementing an interface
{

	JButton button;
	
	public static void main(String[] args) 
	{
       SimpleGui1 gui = new SimpleGui1();
       gui.guiInit();
	}
	
	public void guiInit()
	{
		JFrame frame = new JFrame(); 
		button = new JButton("Click me"); //constructor gets what you want to show on the button
		
		button.addActionListener(this); //this class saying to the button object hey add me to your list of listeners
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //this line makes the program quit
		
		frame.getContentPane().add(button);  // add the button to the frames's content pane
		frame.setSize(300, 300);
		frame.setVisible(true);		
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		button.setText("I have been Clicked");
	}

}
