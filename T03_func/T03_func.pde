
void setup() //happens once
{
  size(600,600);
}

void draw()  //continuously loops
{
  background(0);
  fill(255,0,0);
  drawEllipse(mouseX+100,mouseY+100);
  fill(0,255,0);
  drawEllipse(mouseX+200,mouseY+200);
  fill(0,0,255);
  drawEllipse(mouseX+300,mouseY+300);  

}

void drawEllipse(int x, int y) //custom function
{
  ellipse(x,y,20,20);
}
